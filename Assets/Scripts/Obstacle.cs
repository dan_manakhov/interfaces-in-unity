﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, IClickInterface
{

	public void DoClick() {
		Rigidbody body = GetComponent<Rigidbody>();

		body.AddForce(Vector3.up * 500f);
	}


	public void Jump() {
		Rigidbody body = GetComponent<Rigidbody>();

		body.AddForce(Vector3.up * 500f );
	}
	
}
