﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour, IClickInterface, IClickInterface2
{

	void IClickInterface.DoClick()
	{
		transform.Rotate(0, 45f, 0);
	}

	void IClickInterface2.DoClick()
	{
		transform.Rotate(0, 45f, 0);
	}


	public void TowerRotate() {
		transform.Rotate( 0, 45f, 0 );
	}


	// Update is called once per frame
	void Update () {
		
	}
}
