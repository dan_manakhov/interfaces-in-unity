﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enemy : MonoBehaviour, IClickInterface
{

	public void DoClick()
	{
		StartCoroutine(CMoveForward());
	}


	IEnumerator CMoveForward() {
		for (int i = 0; i < 100; i++)
		{
			transform.Translate(Vector3.forward * 0.05f);
			yield return null;
		}
	}

	public void MoveForward() {
		StartCoroutine(CMoveForward());
	}
}
